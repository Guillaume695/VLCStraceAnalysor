# -*-coding:Utf-8 -*
# /VLCStraceAnalyser.py

import os
import sys
import gc
import threading
import subprocess
import shlex
import time
import numpy
import pandas as pd
import matplotlib.pyplot as plt
from multiprocessing import Process, JoinableQueue, Manager
from termcolor import colored

class AnalyseData():
	def __init__(self, beginning, end, vLCStraceAnalyser):
		self._stop = threading.Event()
		self.beginning = beginning
		self.end = end
		self.VLCStraceAnalyser = vLCStraceAnalyser
		self.cmdList = vLCStraceAnalyser.cmdList
		self.outputDir = vLCStraceAnalyser.outputDir
		self.analysisTasksQueue = vLCStraceAnalyser.analysisTasksQueue
		self.cmdData = {}

	def stop(self):
		self._stop.set()

	def run(self):

		taskNb = self.analysisTasksQueue.get() + 1

		print(colored("AnalyseData: ", "blue")+colored("Process "+str(taskNb)+" is up - Parsing data from command n°"+str(self.beginning+1)+" to n°"+str(self.end), "yellow"))

		for i in range(self.beginning, self.end):
			if self._stop.is_set():
				sys.exit(0)
			# Parse csv output
			cmdName = self.cmdList.loc[i, "NAME"]

			try:
				cmdOutput = pd.read_csv(self.outputDir+"/"+cmdName+".csv")
				# Clean data
				cmdOutput.fillna(0, inplace=True)
				
				# A CHANGER (a cause du nouveau PC)
				path = '"'+os.getcwd().replace("/VLCStrace/src",'')
				cmdOutput.ARG1 = cmdOutput.ARG1.map(lambda x: str(x).replace(path,'"'))
				# cmdOutput.ARG1 = cmdOutput.ARG1.map(lambda x: str(x).replace('"/media/Guillaume/DATA/Documents/VLC','').replace('"',''))
				nbOfCalls = len(cmdOutput)
				pass
			except IOError as e:
				print(colored("ERROR", "red")+colored(" - CSV output file not found ( "+cmdName+".csv not found in "+self.outputDir+" )", "yellow"))
				sys.exit(1)
				
			startingTimestamp = cmdOutput.loc[0].TIMESTAMP
			
			# List of syscalls with filepath in ARG1
			syscallWithFile = list(cmdOutput[[len(str(elem).split('/')) > 1 for elem in cmdOutput.ARG1]].SYSCALL.drop_duplicates().reset_index().SYSCALL)

			# getrandom might be detected as a syscall with a filepath but is actually a false positive
			try:
				syscallWithFile.pop(syscallWithFile.index('getrandom'))
			except ValueError as e:
				pass

			self.cmdData = {"rawCsv": cmdOutput, "data": {}, "startingTimestamp": startingTimestamp, "syscallsTotTime": cmdOutput.sum().loc["ELAPSED"], "nbOfCalls": nbOfCalls, "syscallWithFile": syscallWithFile}

			# Sort each syscall out
			failedTemp = {}
			for syscallGroup, group in self.cmdData["rawCsv"][self.cmdData["rawCsv"].RESULT.astype(str) == "-1"].groupby("SYSCALL"):
				failedTemp[syscallGroup] = {"all": group, "size": len(group), "execTime": group.sum().loc["ELAPSED"]}
			self.cmdData["data"] = {
				"nb": dict(cmdOutput.groupby("SYSCALL").size()),
				"totTime": dict(cmdOutput.groupby("SYSCALL").sum().loc[:,"ELAPSED"]),
				"minExecTime": dict(cmdOutput.groupby("SYSCALL").min().loc[:,"ELAPSED"]),
				"maxExecTime": dict(cmdOutput.groupby("SYSCALL").max().loc[:,"ELAPSED"]),
				"moyExecTime": dict(cmdOutput.groupby("SYSCALL").mean().loc[:,"ELAPSED"]),
				"firstCall": dict(cmdOutput.groupby("SYSCALL").min().loc[:,"TIMESTAMP"] - startingTimestamp),
				"lastCall": dict(cmdOutput.groupby("SYSCALL").max().loc[:,"TIMESTAMP"] - startingTimestamp),
				"failed": failedTemp,
				"orderNbOfCallsList": cmdOutput.groupby("SYSCALL").size().sort_values().loc[::-1].reset_index(),
				"orderExecTimeList": cmdOutput.groupby("SYSCALL").sum().sort_values(by="ELAPSED").loc[::-1].reset_index()
			}

			# Push result to main cmdData
			self.VLCStraceAnalyser.cmdData[cmdName] = self.cmdData

		print(colored("AnalyseData: ", "blue")+colored("Process "+str(taskNb)+" is done!", "green"))
		
		self.analysisTasksQueue.task_done()

class DrawHists():
	def __init__(self, beginning, end, vLCStraceAnalyser):
		self._stop = threading.Event()
		self.beginning = beginning
		self.end = end
		self.VLCStraceAnalyser = vLCStraceAnalyser
		self.cmdData = vLCStraceAnalyser.cmdData
		self.cmdList = vLCStraceAnalyser.cmdList
		self.outputDir = vLCStraceAnalyser.outputDir
		self.histParam = vLCStraceAnalyser.histParam
		self.topNParam = vLCStraceAnalyser.topNParam
		self.outputDir = vLCStraceAnalyser.outputDir
		self.drawingTasksQueue = vLCStraceAnalyser.drawingTasksQueue

	def stop(self):
		self._stop.set()

	def run(self):

		taskNb = self.drawingTasksQueue.get() + 1

		# Create the histogram for each syscall
		for i in range(self.beginning, self.end):
			if self._stop.is_set():
				sys.exit(0)

			cmdName = self.cmdList.loc[i, "NAME"]
			if len(cmdName.split('/')) > 1:
				plottingName = cmdName.split('/')[-1]
			else:
				plottingName = cmdName
			cmdDataTemp = self.cmdData[cmdName]
			startingTimestamp = cmdDataTemp["startingTimestamp"]

			print(colored("DrawHists: ", "blue")+colored("Process "+str(taskNb)+" is up - analysis for ", "yellow")+colored(cmdName, "red")+colored(" ...", "yellow"))

			# Create output folder
			try:
				os.mkdir(self.outputDir+cmdName)
			except Exception as e:
				pass
			try:
				os.mkdir(self.outputDir+cmdName+"/charts")
			except Exception as e:
				pass
			try:
				os.mkdir(self.outputDir+cmdName+"/failedCalls")
			except Exception as e:
				pass
			try:
				os.mkdir(self.outputDir+cmdName+"/filepaths")
			except Exception as e:
				pass
			if (os.path.isfile(self.outputDir+cmdName+"/log.txt")):
				os.remove(self.outputDir+cmdName+"/log.txt")

			# Write log file
			log = open(self.outputDir+cmdName+"/log.txt", "w")
			log.write("VLCTrace - command "+cmdName+"\n----------------------------\n\n")
			log.write("Total nb of syscalls: "+str(cmdDataTemp["nbOfCalls"])+"\n")
			log.write("Total nb of different syscalls: "+str(len(cmdDataTemp["data"]["nb"]))+"\n")
			log.write("Total execution time: "+str(cmdDataTemp["syscallsTotTime"])+"\n\n")
			log.write("Total nb of failed syscalls: "+str(sum([syscallValue["size"] for syscallValue in cmdDataTemp["data"]["failed"].values()]))+"\n")
			log.write("Total execution time of failed syscalls: "+str(sum([syscallValue["execTime"] for syscallValue in cmdDataTemp["data"]["failed"].values()]))+"\n\n")
			log.write("Number of threads: "+str(len(set(cmdDataTemp["rawCsv"].PID)))+"\n\n")
			log.write(cmdDataTemp["rawCsv"].groupby("PID").PID.count().to_string()+"\n\n")

			# Create histograms ----------------------------------------------------------------------------------------
			rk = 0
			for index, row in cmdDataTemp["data"]["orderNbOfCallsList"].iterrows():
				if self._stop.is_set():
					sys.exit(0)
				rk += 1
				
				if cmdDataTemp["data"]["nb"][row["SYSCALL"]] > 1:
					# Calculate X axis intervall
					step = (cmdDataTemp["data"]["lastCall"][row["SYSCALL"]] - cmdDataTemp["data"]["firstCall"][row["SYSCALL"]]) / self.histParam
					xAxis = [ cmdDataTemp["data"]["firstCall"][row["SYSCALL"]] + i * step for i in range(self.histParam)]

					# This variable will store the number of calls in each intervall of length 'step' between first and last call
					nbCallsPerIntervall = list(numpy.zeros(self.histParam, dtype=int))

					# Calculate the number of calls in each intervall
					for rawCall in cmdDataTemp["rawCsv"].loc[cmdDataTemp["rawCsv"]["SYSCALL"] == row["SYSCALL"]].iterrows():
						if self._stop.is_set():
							sys.exit(0)
						# calculte the index of the intervall
						if step != 0:
							indexTemp = min(int((rawCall[1].TIMESTAMP - startingTimestamp - cmdDataTemp["data"]["firstCall"][row["SYSCALL"]]) / step), self.histParam - 1)
						else:
							indexTemp = 0
						# Add entry in nbCallsPerIntervall
						nbCallsPerIntervall[indexTemp] += 1

					# Create bar chart
					figTemp = plt.figure(figsize=(10, 8), dpi=100)
					ax = plt.gca()
					ax.bar(xAxis, nbCallsPerIntervall, step, color="blue")
					ax.set_ylim(top=1.1*max(nbCallsPerIntervall)) # nice plot purposes only
					ax.set_xlabel('seconds')
					ax.set_ylabel('count')

					if row["SYSCALL"] is not None:
						titletmp = row["SYSCALL"]+ ' (' + str(cmdDataTemp["data"]["nb"][row["SYSCALL"]]) + ' calls)\n'+plottingName+'\n'
					else:
						titletmp = ''
					titletmp = titletmp+" call time distribution"
					figTemp.suptitle(titletmp)

					# Save plot
					filename = plottingName+"_"+row["SYSCALL"]+".png"
					figTemp.savefig(self.outputDir+cmdName+"/charts/"+filename)
					
					# Free memory before the end of life :D
					figTemp.clf()
					plt.close()
				# else:
				# 	print colored("VLCStraceAnalyser: ", "blue")+colored("Skipping plot for ", "yellow")+colored(row["SYSCALL"], "red")+colored(" since there is only one value.", "yellow")

				# Create output summary
				log.write("#"+str(rk)+" - "+row["SYSCALL"]+"\n"+"".join(['-' for i in range(len("#"+str(rk)+" - "+row["SYSCALL"]))])+"\n\n")
				log.write("Nb of calls: "+str(cmdDataTemp["data"]["nb"][row["SYSCALL"]])+" ("+str(int(cmdDataTemp["data"]["nb"][row["SYSCALL"]]*100/cmdDataTemp["nbOfCalls"]))+"%)\n")
				if not("exit" in row["SYSCALL"]):
					log.write("Total execution time: "+str(cmdDataTemp["data"]["totTime"][row["SYSCALL"]])+" ("+str(int(cmdDataTemp["data"]["totTime"][row["SYSCALL"]]/cmdDataTemp["syscallsTotTime"]*100))+"%)\n")
				else:
					log.write("Total execution time: "+str(cmdDataTemp["data"]["totTime"][row["SYSCALL"]])+"\n")
				log.write("Average execution time per call: "+str(cmdDataTemp["data"]["moyExecTime"][row["SYSCALL"]])+"\n")
				log.write("min / max execution time: "+str(cmdDataTemp["data"]["minExecTime"][row["SYSCALL"]])+" / "+str(cmdDataTemp["data"]["maxExecTime"][row["SYSCALL"]])+"\n")
				log.write("first / last call: "+str(cmdDataTemp["data"]["firstCall"][row["SYSCALL"]])+" / "+str(cmdDataTemp["data"]["lastCall"][row["SYSCALL"]])+"\n")
				if (row["SYSCALL"] in cmdDataTemp["data"]["failed"]):
					log.write("Number of failed calls: "+str(len(cmdDataTemp["data"]["failed"][row["SYSCALL"]]))+"\n\n")
				else:
					log.write("\n")

			# --------------------------------------------------------------------------------------------------

			# Create top n charts
			n = min(self.topNParam, len(cmdDataTemp["data"]["orderNbOfCallsList"]))
			# Top n in nb of calls ----------------------------------------------------------------------------------------------------
			figTemp = plt.figure(figsize=(n, 8), dpi=200)
			ax = plt.gca()
			topData = list(cmdDataTemp["data"]["orderNbOfCallsList"].loc[:, 0])[0:n]
			bars = ax.bar([k for k in range(1, n+1)], topData, 0.75, color="blue")
			ax.set_ylim(top=1.2*max(topData)) # nice plot purposes only
			ax.set_xlabel('syscall')
			ax.set_ylabel('count')

			def autolabel(rects):
				# Attach a text label above each bar displaying its height
				l = 0
				for rect in rects:
					height = rect.get_height()
					ax.text(rect.get_x() + rect.get_width()/2., (1.01)*height, cmdDataTemp["data"]["orderNbOfCallsList"].loc[l].SYSCALL+"\n"+str(int(height)), ha='center', va='bottom')
					l += 1
			autolabel(bars)

			filename = plottingName+"_topN_nbOfCalls.png"
			figTemp.suptitle("Top "+str(self.topNParam)+" syscalls - Number of calls\n"+plottingName+"\n(tot: "+str(cmdDataTemp["nbOfCalls"])+")")
			figTemp.savefig(self.outputDir+cmdName+"/"+filename)
			
			# Free memory before the end of life :D
			figTemp.clf()
			plt.close()
			# ---------------------------------------------------------------------------------------------------

			# Top n in execution time -----------------------------------------------------------------------------------------------------
			figTemp = plt.figure(figsize=(n, 8), dpi=200)
			ax = plt.gca()
			topData = list(cmdDataTemp["data"]["orderExecTimeList"].loc[:, 'ELAPSED'])[0:n]
			bars = ax.bar([k for k in range(1, n+1)], topData, 0.75, color="blue")
			ax.set_ylim(top=1.2*max(topData)) # nice plot purposes only
			ax.set_xlabel('syscall')
			ax.set_ylabel('Total execution time (seconds)')

			def autolabel2(rects):
				# Attach a text label above each bar displaying its height
				l = 0
				for rect in rects:
					height = rect.get_height()
					ax.text(rect.get_x() + rect.get_width()/2., (1.01)*height, cmdDataTemp["data"]["orderExecTimeList"].loc[l].SYSCALL+"\n"+str(height), ha='center', va='bottom')
					l += 1
			autolabel2(bars)

			filename = plottingName+"_topN_execTime.png"
			figTemp.suptitle("Top "+str(self.topNParam)+" syscalls - Execution time\n"+plottingName+"\n(tot: "+str(cmdDataTemp["syscallsTotTime"])+")")
			figTemp.savefig(self.outputDir+cmdName+"/"+filename)
			
			# Free memory before the end of life :D
			figTemp.clf()
			plt.close()
			# ----------------------------------------------------------------------------------------------------
						
			# Handles failed syscalls ------------------------------------------------------------------------------------------------------
			# Pie chart - Nb of calls
			labelsTemp = cmdDataTemp["data"]["failed"].keys()
			labels = labelsTemp+[]
			for i in range(len(labels)):
				label = labels[i]
				labels[i] = label+" ("+str(cmdDataTemp["data"]["failed"][label]["size"])+")"
			nbTotFailedSyscalls = sum([syscallValue["size"] for syscallValue in cmdDataTemp["data"]["failed"].values()])
			values = [cmdDataTemp["data"]["failed"][syscallName]["size"]*100/nbTotFailedSyscalls for syscallName in labelsTemp]
			figTemp = plt.figure(figsize=(10, 8), dpi=200)
			ax = plt.gca()
			ax.pie(values, labels=labels, autopct='%1.1f%%', shadow=False, startangle=90)
			ax.axis("equal")
			filename = plottingName+"_failedSyscalls_nbOfCalls.png"
			figTemp.suptitle("Failed syscalls - Number of calls\n"+plottingName+"\n(tot: "+str(nbTotFailedSyscalls)+")")
			figTemp.savefig(self.outputDir+cmdName+"/failedCalls/"+filename)
			
			# Free memory before the end of life :D
			figTemp.clf()
			plt.close()

			# Pie chart - Exec time wasted
			labels = labelsTemp+[]
			for i in range(len(labels)):
				label = labels[i]
				labels[i] = label+" ("+str(cmdDataTemp["data"]["failed"][label]["execTime"])+" s)"
			totExecTimeFailedSyscalls = sum([syscallValue["execTime"] for syscallValue in cmdDataTemp["data"]["failed"].values()])
			values = [cmdDataTemp["data"]["failed"][syscallName]["execTime"]*100/totExecTimeFailedSyscalls for syscallName in labelsTemp]
			figTemp = plt.figure(figsize=(10, 8), dpi=200)
			ax = plt.gca()
			ax.pie(values, labels=labels, autopct='%1.1f%%', shadow=False, startangle=90)
			ax.axis("equal")
			filename = plottingName+"_failedSyscalls_execTime.png"
			figTemp.suptitle("Failed syscalls - Execution time\n"+plottingName+"\n(tot: "+str(totExecTimeFailedSyscalls)+" s)")
			figTemp.savefig(self.outputDir+cmdName+"/failedCalls/"+filename)
			
			# Free memory before the end of life :D
			figTemp.clf()
			plt.close()

			# Save ARG1 for each failed call 8533 input start
			for syscallName, syscallGroup in cmdDataTemp["data"]["failed"].items():
				if self._stop.is_set():
					sys.exit(0)
				try:
					os.mkdir(self.outputDir+cmdName+"/failedCalls/"+syscallName)
				except Exception as e:
					pass
				syscallGroup["all"].loc[:, "ARG1"].to_csv(self.outputDir+cmdName+"/failedCalls/"+syscallName+"/"+syscallName+".csv")
				
				# Analyse filepaths for each syscall (if it has some)
				if syscallName in cmdDataTemp["syscallWithFile"]:
					# Generate tree of files called by this syscall
					treeTemp = self.generateTree(syscallGroup["all"], syscallName+" - failed calls", plottingName)
					
					# Calculate the nb of files per folder in tree
					countTreeTemp = self.countFilesPerNode(treeTemp, syscallName+" - failed calls", plottingName)
					
					# Generate pie charts for tree
					self.treeToPieCharts(countTreeTemp, "Failed", "failedCalls", syscallName, cmdName, 0, '', plottingName)
					
					# Free memory
					del treeTemp, countTreeTemp
							
			# ----------------------------------------------------------------------------------------------------
			
			# Draw filepaths pie charts for al syscalls ----------------------------------------------------------
			for syscallName, group in cmdDataTemp["rawCsv"][[elem in cmdDataTemp["syscallWithFile"] for elem in cmdDataTemp["rawCsv"].SYSCALL]].groupby("SYSCALL"):
				try:
					os.mkdir(self.outputDir+cmdName+"/filepaths/"+syscallName)
				except Exception as e:
					pass
				group.loc[:, "ARG1"].to_csv(self.outputDir+cmdName+"/filepaths/"+syscallName+"/"+syscallName+".csv")
				
				# Generate tree of files called by this syscall
				treeTemp = self.generateTree(group, syscallName, plottingName)
				
				# Calculate the nb of files per folder in tree
				countTreeTemp = self.countFilesPerNode(treeTemp, syscallName, plottingName)
				
				# Generate pie charts for tree
				self.treeToPieCharts(countTreeTemp, "/", "filepaths", syscallName, cmdName, 0, '', plottingName)
				
				# Free memory
				del treeTemp, countTreeTemp
			# ----------------------------------------------------------------------------------------------------

			log.close()
			print(colored("DrawHists: ", "blue")+colored("Process "+str(taskNb)+" - Analysis for ", "green")+colored(plottingName, "green")+colored(" done!", "green"))
				
		self.drawingTasksQueue.task_done()
			
	def generateTree(self, syscallGroup, label, cmdName):
		# Create a tree leading to each file called by each syscall
		# For each level:
		#				root folder Name			   <-  Dict key
		#			   /				\
		#	 [ list of filenames  ,   list of dict ]   <-  Dict value
	
		print(colored("generateTree: ", "blue")+colored("Generating tree for ", "yellow")+colored(label+" - "+cmdName, "red")+colored(" ...", "yellow"))
		
		tree = [[], {}]
		for syscall in syscallGroup.iterrows():
			syscallFilePath = syscall[1].ARG1.split('/')[1::]
			node = tree
			parentNode = tree
			while len(syscallFilePath) > 1:
				if syscallFilePath[0] in node[1]:
					node = node[1][syscallFilePath[0]]
					syscallFilePath.pop(0)
				else:
					node[1][syscallFilePath[0]] = [[], {}]
					node = node[1][syscallFilePath[0]]
					syscallFilePath.pop(0)
			node[0].append(syscallFilePath[0])
		print(colored("simplifyTree: ", "blue")+colored("Simplifying tree for ", "yellow"))+colored(label+" - "+cmdName, "red")+colored(" ...", "yellow")
		self.simplifyTree(tree[1])
		return tree
	
	
	
	# DEF simplifyTree
	#
	# Simplifies tree so that folder names with only 1 folder are put together
	# For example:
	#	   [[], {'deb': [[], {'cou1': [[], {'cou2bis': [['alpha.txt'], {}], 'cou2': [[], {'pp': [['gamma.txt'], {}]}]}]}]}]
	#  ==>  [[], {'deb/cou1': [[], {'cou2bis': [['alpha.txt'], {}], 'cou2/pp': [['gamma.txt'], {}]}]}]
	
	def simplifyTree(self, tree):
			
		for rootName, subtree in tree.items():
			if subtree[1] != {}:
				if subtree[0] == []:
					self.simplifyTree(subtree[1])
					if len(subtree[1]) == 1:
						for nodeName, subsubtree in subtree[1].items():
							tree[rootName+"/"+nodeName] = [subsubtree[0], subsubtree[1]]
						del tree[rootName]
	
	
	# DEF countFilesPerNode
	#
	# Counts the number of calls to a file contained in each folder recursively, and the number of different files
	# For example:
	#	  [[], {'deb/cou1': [[beta.txt], {'cou2bis': [['alpha.txt'], {}], 'cou2/pp': [['gamma.txt'], {}]}]}]
	#  ==> [3, {'deb/cou1': [3, {'cou2bis': [1, {}], 'cou2/pp': [1, {}]}]}]
	
	def countFilesPerNode(self, tree, label, cmdName):
						
		print(colored("countFilesPerNode: ", "blue")+colored("Counting files for ", "yellow")+colored(label+" - "+cmdName, "red")+colored(" ...", "yellow"))
		
		def countFilesRec(countTree, tree):
			if tree[1] != {}:
				for rootName, subtree in tree[1].items():
					countTree[1][rootName] = [len(subtree[0]), {}, len(set(subtree[0]))]
					countFilesRec(countTree[1][rootName], subtree)
				for name, subCountTree in countTree[1].items():
					countTree[0] += subCountTree[0]
					countTree[2] += subCountTree[2]
			else:
				countTree[0] = len(tree[0])
				countTree[2] = len(set(tree[0]))
		countTreeTemp = [0, {}, 0]
		countFilesRec(countTreeTemp, tree)
		return countTreeTemp
	
	
	# DEF countFilesPerNode
	#
	# Plot pie charts for each depth of countTree with maximum depth of `depth` or end of tree
	
	def treeToPieCharts(self, countTree, title, folder, syscallName, cmdName, depth, filenamePrefix, plottingName):
		if countTree[1] != {}:
			# Create list for pie chart
			tot = countTree[0]
			pieChartData = []
			labels = []
			for folderName, node in countTree[1].items():
				labels.append(folderName+" ("+str(node[0])+" / "+str(node[2])+")")
				pieChartData.append(node[0]*100/tot)
			figTemp = plt.figure(figsize=(10, 8), dpi=200)
			ax = plt.gca()
			ax.pie(pieChartData, labels=labels, autopct='%1.1f%%', shadow=False, startangle=90)
			ax.axis("equal")
			filename = filenamePrefix+folder+"_"+syscallName+".png"
			figTemp.suptitle("Filepaths - "+syscallName+" - "+title+"\n"+plottingName+"\n(tot calls: "+str(tot)+" / tot files: "+str(countTree[2])+")")
			try:
				os.mkdir(self.outputDir+cmdName+"/"+folder+"/"+syscallName+"/depth_"+str(depth))
			except Exception as e:
				pass
			figTemp.savefig(self.outputDir+cmdName+"/"+folder+"/"+syscallName+"/depth_"+str(depth)+"/"+filename)
			
			# Free memory before the end of life :D
			figTemp.clf()
			plt.close()
			
			# for each subtree, plot pie charts
			for folderName, node in countTree[1].items():
				self.treeToPieCharts(node, title+"/"+folderName, folder, syscallName, cmdName, depth+1, folderName.replace('/', '-')+'_', plottingName)
									
						

class VLCStraceAnalyser():
	def __init__(self, cmdList, outputDir, histParam, numberOfProcess, topNParam, drawing, identify, seqdiag):
		manager = Manager()
		self.cmdList = cmdList
		self.outputDir = outputDir
		self.histParam = histParam
		self.numberOfProcess = numberOfProcess
		self.topNParam = topNParam
		self.drawing = drawing
		self.identify = identify
		self.seqdiag = seqdiag
		self.analysisProcesses = []
		self.analysisTasksQueue = manager.JoinableQueue()
		self.drawingProcesses = []
		self.drawingTasksQueue = manager.JoinableQueue()
		self.cmdData = manager.dict()

	def start(self):
			
		self.time = time.time()

		if self.identify:
			# Phase 1: tries to identify syscall logs of each command
			print(colored("VLCStraceAnalyser: ", "blue")+colored("Phase 1 - trying to identify syscall logs of each command ...", "yellow"))
			commonFields = ['SYSCALL', 'CATEGORY', 'SPLIT', 'ARGC', 'ARG1', 'ARG2', 'ARG3', 'ARG4', 'ARG5', 'ARG6', 'RESULT']
			droppedDict = {}
			newCmds = []
			
			# Drop duplicates of each command
			for cmd in self.cmdList.iterrows():
				dataTemp = pd.read_csv(self.outputDir+"/"+cmd[1].NAME+".csv")
				droppedDict[cmd[1].NAME] = dataTemp.loc[:, commonFields].drop_duplicates()
			
			for currentCmd in self.cmdList.iterrows():
				# For each currentCmd of weight x, find new calls comparred to cmd of weight y < x 
				for cmd in self.cmdList[(self.cmdList.WEIGHT < currentCmd[1].WEIGHT) & (self.cmdList.CAT == currentCmd[1].CAT)].iterrows():
					# New calls are the result of a left merge where you keep only what isn't in both dataframes (using the dropped dataframes)
					mergedLeft = droppedDict[currentCmd[1].NAME].merge(droppedDict[cmd[1].NAME], how='left', indicator=True)
					# Save the `left_only` in a new file
					pd.read_csv(self.outputDir+"/"+currentCmd[1].NAME+".csv").loc[mergedLeft[mergedLeft['_merge']=='left_only'].index].to_csv(self.outputDir+"/"+currentCmd[1].NAME+"_ISOLATED_FROM_"+cmd[1].NAME+".csv")
					# Prepare new cmd (will be added to cmdList for analysis in phase 2)
					newCmds.append({"NAME": currentCmd[1].NAME+"_ISOLATED_FROM_"+cmd[1].NAME, "ARGS": currentCmd[1].ARGS, "WEIGHT": currentCmd[1].WEIGHT, "CAT": currentCmd[1].CAT})
					
			# Add new commands to cmdList so that newly computed csv will be analysed in phase 3
			for newCmd in newCmds:
				self.cmdList.loc[len(self.cmdList)] = [newCmd["NAME"], newCmd["ARGS"], newCmd["WEIGHT"], newCmd["CAT"]]

		if self.seqdiag:
			# Phase 2.1: Isolates syscalls for each thread of each cmd.
			print(colored("VLCStraceAnalyser: ", "blue")+colored("Phase 2.1 - isolating syscalls for each thread of each cmd ...", "yellow"))
			newCmds = []
			
			for currentCmd in self.cmdList.iterrows():
				try:
					os.mkdir(self.outputDir+"/"+currentCmd[1].NAME)
				except Exception as e:
					pass
				try:
					os.mkdir(self.outputDir+"/"+currentCmd[1].NAME+"/threads")
				except Exception as e:
					pass
				dataTemp = pd.read_csv(self.outputDir+"/"+currentCmd[1].NAME+".csv")
				# For each command, save in a new csv file the syscalls for each thread
				for pid, pidgroup in dataTemp.groupby("PID"):
					pidgroup.to_csv(self.outputDir+"/"+currentCmd[1].NAME+"/threads/"+currentCmd[1].NAME+"_"+str(pid)+".csv")
					newCmds.append({"NAME": currentCmd[1].NAME+"/threads/"+currentCmd[1].NAME+"_"+str(pid), "ARGS": currentCmd[1].ARGS, "WEIGHT": currentCmd[1].WEIGHT, "CAT": currentCmd[1].CAT})
			
			# Add new commads to cmdlist so that newly computed csv will be analysed in phase 3
			for newCmd in newCmds:
				self.cmdList.loc[len(self.cmdList)] = [newCmd["NAME"], newCmd["ARGS"], newCmd["WEIGHT"], newCmd["CAT"]]

			# Phase 2.2: generates sequence diagrams for all threads
			print(colored("VLCStraceAnalyser: ", "blue")+colored("Phase 2.2 - generating sequence diagrams for all threads of all cmd ...", "yellow"))

			# Creates sequence diag for each cmd
			for currentCmd in self.cmdList.iterrows():
				# only do this for main commands (and not for thread ones)
				if not("_" in currentCmd[1].NAME):
					dataTemp = pd.read_csv(self.outputDir+"/"+currentCmd[1].NAME+".csv")
					timeOrigin = float(dataTemp.loc[0].TIMESTAMP)
					# clone is the list of syscalls creating a new thread
					clone = dataTemp.loc[dataTemp.SYSCALL == "clone"]
					clone.fillna(0)
					seqDiagData = []
					pidName = {int(dataTemp.loc[0].PID): "Main"}
					parentCounters = {}

					# creates the data structure that will create the sequence diagram
					for elemId, elem in clone.iterrows():
						if elem.PID in parentCounters:
							parentCounters[elem.PID] += 1
						else:
							parentCounters[elem.PID] = 1
						seqDiagData.append({"parentThID": elem.PID, "sonThID": elem.RESULT, "nthChild": parentCounters[elem.PID], "timestamp": (float(elem.TIMESTAMP) - timeOrigin), "entryType": "clone"})
						thExitCall = dataTemp.loc[(dataTemp.SYSCALL.astype(str) == "EXIT") & (dataTemp.PID.astype(str) == elem.RESULT)].reset_index()
						if len(thExitCall) > 0:
							thExitCall = thExitCall.loc[0]
							seqDiagData.append({"parentThID": elem.PID, "sonThID": elem.RESULT, "nthChild": parentCounters[elem.PID], "timestamp": (float(thExitCall.TIMESTAMP) - timeOrigin), "entryType": "exit"})
					# sort by timestamp
					seqDiagData = sorted(seqDiagData, key=lambda x: x['timestamp'])
					# write seqdiag file
					seqDiagFile = open(self.outputDir+currentCmd[1].NAME+"/sequenceDiag.diag", "w")
					# Check if the auto preparser and audio are activated
					autoPreparser = True
					audio = True
					if "--no-auto-preparse" in currentCmd[1].ARGS:
						autoPreparser = False
					if "--no-audio" in currentCmd[1].ARGS:
						audio = False
					seqDiagFile.write("seqdiag {\n")
					# For each entry add ligne to seqdiagFile with the correct syntax
					for entry in seqDiagData:
						if int(entry["parentThID"]) in pidName:
							parentName = pidName[int(entry["parentThID"])]
							if parentName == "Main":
								if entry["nthChild"] == 1:
									pidName[int(entry["sonThID"])] = "AudioOutput"
								if entry["nthChild"] == 2:
									pidName[int(entry["sonThID"])] = "Playlist"
								if entry["nthChild"] == 3:
									pidName[int(entry["sonThID"])] = "IntfCreate"
								if entry["nthChild"] == 4:
									if autoPreparser:
										pidName[int(entry["sonThID"])] = "AutoPreparser"
									else:
										pidName[int(entry["sonThID"])] = "IntfCreate"
								if entry["nthChild"] == 5:
									pidName[int(entry["sonThID"])] = "IntfCreate"
							elif parentName == "AutoPreparser":
								if entry["nthChild"] == 1:
									pidName[int(entry["sonThID"])] = "PreparserOpenInput"
								if entry["nthChild"] >= 2:
									pidName[int(entry["sonThID"])] = "InputPreparserWorker"
							elif parentName == "Playlist":
								if entry["nthChild"]%2:
									pidName[int(entry["sonThID"])] = "Input"
								else:
									pidName[int(entry["sonThID"])] = "InputPreparserWorker"
							elif parentName == "InputPreparserWorker":
								if entry["nthChild"] == 1:
									pidName[int(entry["sonThID"])] = "StartWorkerSearchLocal"
							elif parentName == "StartWorkerSearchLocal":
								pidName[int(entry["sonThID"])] = "SearchLocal"
							elif parentName == "SearchLocal":
								pidName[int(entry["sonThID"])] = "StartWorkerSearchNetwork"
							elif parentName == "Input":
								pidName[int(entry["sonThID"])] = "Decoder"
							elif parentName == "Decoder":
								if entry["nthChild"] == 1:
									pidName[int(entry["sonThID"])] = "VoutWindowNew"
								if entry["nthChild"] == 2:
									pidName[int(entry["sonThID"])] = "VoutUpdateFormat"
						else:
							parentName = ""
						if int(entry["sonThID"]) in pidName:
							sonName = pidName[int(entry["sonThID"])]
						else:
							sonName = ""

						if entry["entryType"] == "clone":
							seqDiagFile.write(parentName+str("-"*(len(parentName) > 0))+str(entry["parentThID"])+"  -> "+sonName+str("-"*(len(sonName) > 0))+str(entry["sonThID"])+' [label = "'+str(entry["timestamp"])+'"];\n')

						if entry["entryType"] == "exit":
							seqDiagFile.write(parentName+str("-"*(len(parentName) > 0))+str(entry["parentThID"])+" <-- "+sonName+str("-"*(len(sonName) > 0))+str(entry["sonThID"])+' [label = "'+str(entry["timestamp"])+'"];\n')

					seqDiagFile.write("}")
					seqDiagFile.close()
					# launch seqDiag
					subprocess.Popen(shlex.split("seqdiag -Tsvg "+self.outputDir+currentCmd[1].NAME+"/sequenceDiag.diag"))

		# Phase 3: generates histograms and stats for each command
		print(colored("VLCStraceAnalyser: ", "blue")+colored("Phase 3.1 - Starting analysis for each command ...", "yellow"))

		nbCmd = len(self.cmdList)
		# Parse Data
		nbProcessAnalysis = min(self.numberOfProcess, nbCmd)
		# Nb of command to be handled by each process
		cmdPerProcess = int(nbCmd / nbProcessAnalysis)
		for proNb in range(nbProcessAnalysis):
			# Each process handles cmd n°proNb*cmdPerProcess to n°(proNb+1)*cmdPerProcess
			if (proNb == nbProcessAnalysis - 1):
				end = nbCmd
			else:
				end = (proNb+1)*cmdPerProcess
			analysisProcess = AnalyseData(proNb*cmdPerProcess, end, self)
			self.analysisProcesses.append(analysisProcess)
			self.analysisTasksQueue.put(proNb)
				
		# Start analysis processes
		for analysisProcess in self.analysisProcesses:
			current_process = Process(target=analysisProcess.run)
			current_process.start()
				
		# Wait for all analysis processes to end
		self.analysisTasksQueue.join()
		
		print(colored("VLCStraceAnalyser: ", "blue")+"Analysis took: "+str(time.time()-self.time)+" secondes.")

		if self.drawing:
			print(colored("VLCStraceAnalyser: ", "blue")+colored("Phase 3.1 - Plotting diagrams and pie charts for each command ...", "yellow"))
			# Draw histograms for parsed data
			nbProcessDrawing = min(self.numberOfProcess, nbCmd)
			# Nb of command to be handled by each process
			cmdPerProcess = int(nbCmd / nbProcessDrawing)
			for proNb in range(nbProcessDrawing):
				# Each process handles cmd n°proNb*cmdPerProcess to n°(proNb+1)*cmdPerProcess
				if (proNb == nbProcessDrawing - 1):
						end = nbCmd
				else:
						end = (proNb+1)*cmdPerProcess
				drawingProcess = DrawHists(proNb*cmdPerProcess, end, self)
				self.drawingProcesses.append(drawingProcess)
				self.drawingTasksQueue.put(proNb)
			
			# Start drawing processes
			for drawingProcess in self.drawingProcesses:
				current_process = Process(target=drawingProcess.run)
				current_process.start()

			# Wait for all drawing threads to end
			self.drawingTasksQueue.join()
				
			print(colored("VLCStraceAnalyser: ", "blue")+"Drawing took: "+str(time.time()-self.time)+" secondes.")

		return
