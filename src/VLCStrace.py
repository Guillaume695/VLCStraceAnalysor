# -*-coding:Utf-8 -*
# /init.py

"""\
NAME
    VLCStraceGeneration - generates strace logs & stats for VLC

DESCRIPTION
    Automatically generates strace logs & stats on different VLC use cases. You
	need to provide a csv file in which you list all the optionnal arguments you
	wish to launch VLC with.

USAGE
	VLCStrace.py [-h] [-o OUTPUT] [-v VLC] [-c COMMANDS] [-t TIMEOUT] [-a]
                    [-p HISTPARAM]

OPTIONS
	-h, --help            show this help message and exit
        -o OUTPUT, --output OUTPUT
                                Output folder. The name provided here cannot be a
                                folder that already exists.
        -v VLC, --vlc VLC     Path to vlc executable
        -c COMMANDS, --commands COMMANDS
                                CSV commands list. You should only put in this file
                                the args that will be appended to the call to VLC.
                                This CSV must have a 'NAME' and a 'ARGS' column.
        -t TIMEOUT, --timeout TIMEOUT
                                Strace capture timeout for each command
        -a, --analyse         Only analyse data. This option considers that the
                                syscall logs have already been generated. Please
                                specify (-o) option.
        -d, --drawing         Draw histograms and pie charts.
        -p HISTPARAM, --histParam HISTPARAM
                                Number of chart bars for histograms
        -b TOPNPARAM, --topNParam TOPNPARAM
                                Number calls to display in top N charts
        -n NUMBEROFPROCESSES, --numberOfProcesses NUMBEROFPROCESSES
                                Number of processes used to compute the data. Default
                                value is the number of CPUs.

REQUIREMENTS
    python 2.7
    numpy
    matplotlib
    termcolor
    pandas

"""

import os
import sys
import random
import string
import argparse
import time
import pandas as pd
import multiprocessing
from termcolor import colored
from VLCStraceGenerator import VLCStraceGenerator
from VLCStraceAnalyser import VLCStraceAnalyser

NAME = "VLCStraceGeneration - generates strace logs & stats for VLC"
DRESCRIPTION = "Automatically generates strace logs & stats on different VLC use cases. You need to provide a csv file in which you list all the optionnal arguments you wish to launch VLC with."
TIME_START = time.time()

def main():
	parser = argparse.ArgumentParser(description=DRESCRIPTION)
	parser.add_argument('-o', '--output', default="", help="Output folder. The name provided here cannot be a folder that already exists.")
	parser.add_argument('-v', '--vlc', default="../../../vlc/build3/vlc", help="Path to vlc executable")
	parser.add_argument('-c', '--commands', default="commands.csv", help="CSV commands list. You should only put in this file the args that will be appended to the call to VLC. This CSV must have a 'NAME' and a 'ARGS' column.")
	parser.add_argument('-t', '--timeout', default=7, help="Strace capture timeout for each command")
	parser.add_argument('-a', '--analyse', action='store_true', default=False, help="Only analyse data. This option considers that the syscall logs have already been generated. Please specify (-o) option.")
	parser.add_argument('-i', '--identify', action='store_true', default=False, help="Identifies syscall logs of each command (Phase 1).")
	parser.add_argument('-s', '--seqdiag', action='store_true', default=False, help="Creates sequence diagrams for the thread of each command and isolates syscalls of each thread. (Phase 2).")
	parser.add_argument('-d', '--drawing', action='store_true', default=False, help="Draw histograms and pie charts (Phase 3).")
	parser.add_argument('-p', '--histParam', default=10, help="Number of chart bars for histograms")
	parser.add_argument('-b', '--topNParam', default=15, help="Number calls to display in top N charts")
	parser.add_argument('-n', '--numberOfProcesses', default=multiprocessing.cpu_count(), help="Number of processes used to compute the data. Default value is the number of CPUs.")
	args = parser.parse_args()
	
	print(colored("VLCStraceGeneration: ", "blue")+colored("Initialization ...", "white"))
	# Test on output folder
	if (args.output != ""):
		if (args.output[-1] != "/"):
			args.output = args.output+"/"
		if (args.analyse == False):
			try:
				os.mkdir('../output/'+args.output)
				pass
			except OSError as e:
				print(colored("ERROR", "red")+colored(" - This folder already exists. Please choose another name. Check (-h) for help.", "yellow"))
				sys.exit(1)
		else:
			if not(os.path.isdir('../output/'+args.output)):
				print(colored("ERROR", "red")+colored(" - The specified folder doesn't exist. Please choose another name. Check (-h) for help.", "yellow"))
				sys.exit(1)
	else:
		# Generates a random 10 chars folder name
		args.output = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10))+"/"
		new = False
		while not(new):
			try:
				os.mkdir('../output/'+args.output)
				new = True
				pass
			except OSError as e:
				args.output = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10))
		print(colored("VLCStraceGeneration: ", "blue")+colored("Output will be saved in /output/"+args.output, "white"))

	# Test on vlc path
	if not(os.path.isfile(args.vlc)):
		print(colored("ERROR", "red")+colored(" - Path to VLC incorrect. Please try again.", "yellow"))
		sys.exit(1)

	# Test on timeout
	try:
		args.timeout = int(args.timeout)
		pass
	except ValueError as e:
		print(colored("ERROR", "red")+colored(" - Timeout (-t) must be a number. Check -h for help.", "yellow"))
		sys.exit(1)

	# Test on histParam
	try:
		args.histParam = int(args.histParam)
		pass
	except ValueError as e:
		print(colored("ERROR", "red")+colored(" - HistParam (-p) must be a number. Check -h for help.", "yellow"))
		sys.exit(1)

	# Test on topNParam
	try:
		args.topNParam = int(args.topNParam)
		pass
	except ValueError as e:
		print(colored("ERROR", "red")+colored(" - topNParam (-b) must be a number. Check -h for help.", "yellow"))
		sys.exit(1)

	# Test on numberOfProcesses
	try:
		args.numberOfProcesses = int(args.numberOfProcesses)
		pass
	except ValueError as e:
		print(colored("ERROR", "red")+colored(" - numberOfProcesses (-n) must be a number. Check -h for help.", "yellow"))
		sys.exit(1)

	# Test on commands list
	if (args.commands == ""):
		print(colored("ERROR", "red")+colored(" - No commands list specified (-c). Check (-h) for help.", "yellow"))
		sys.exit(1)
	else:
		# Read commands list
		try:
			cmdList = pd.read_csv(args.commands)
			pass
		except IOError as e:
			print(colored("ERROR", "red")+colored(" - File not found. Check -h for help.", "yellow"))
			sys.exit(1)
		print(colored("VLCStraceGeneration: ", "blue")+colored("Ready to go!", "green"))
		if not(args.analyse):
			# Creates strace logs for all commands in commandes.py
			vlcStraceGenerator = VLCStraceGenerator(cmdList, '../output/'+args.output, args.vlc, args.timeout)
			vlcStraceGenerator.start()
		# Analyse collected data
		vlcStraceAnalyser = VLCStraceAnalyser(cmdList, '../output/'+args.output, args.histParam, args.numberOfProcesses, args.topNParam, args.drawing, args.identify, args.seqdiag)
		try:
			vlcStraceAnalyser.start()
		except KeyboardInterrupt as e:
			print(colored("VLCStraceAnalyser: ", "blue")+colored("Keyboard Interrupt - Waiting for all processes to stop ...", "red"))
			for process in vlcStraceAnalyser.analysisProcesses:
				process.stop()
                        for process in vlcStraceAnalyser.drawingProcesses:
				process.stop()
			sys.exit(0)

	return

if __name__ == "__main__":
	main()
	print(colored("VLCStraceAnalyser: ", "blue")+"Execution time: "+str(time.time()-TIME_START)+" secondes.")
