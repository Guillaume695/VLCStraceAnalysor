# -*-coding:Utf-8 -*
# /VLCStraceGenerator.py

import time
import os
from termcolor import colored

import subprocess
import shlex

class VLCStraceGenerator():
	def __init__(self, cmdList, outputDir, vlc, commandTimeout):
                self.time = time.time()
		self.cmdList = cmdList
		self.outputDir = outputDir
		self.vlc = vlc
		self.commandTimeout = commandTimeout

	def start(self):
		nbCmd = len(self.cmdList)

		for i in range(nbCmd):
			
			cmdName = self.cmdList.loc[i, "NAME"]
			cmdArgs = self.cmdList.loc[i, "ARGS"]
			print(colored("VLCStraceGenerator: ", "blue")+colored("Launching strace for ", "yellow")+colored(cmdName, "red")+colored(" command ...", "yellow"))
			
			# Create output folder according to command i name
			if (os.path.isdir(self.outputDir+cmdName)):
				os.mkdir(self.outputDir+cmdName)
			
			# Create strace logs for command i in cmdList
			subprocess.Popen(shlex.split('strace -f -ttt -T -o '+self.outputDir+'/'+cmdName+'.out '+self.vlc+' '+cmdArgs))
			
			# Wait 5 secondes, then kill strace to end strace analysis
			print(colored("VLCStraceGenerator: ", "blue")+colored("Waiting "+str(self.commandTimeout)+" secondes ...", "yellow"))
			time.sleep(self.commandTimeout)
			subprocess.call(shlex.split('pkill strace'))
			subprocess.call(shlex.split('pkill vlc'))

			# Convert strace output to csv file
			print(colored("VLCStraceGenerator: ", "blue")+colored("Creating csv ouput ...", "yellow"))
			subprocess.call(shlex.split("python modules/pystrace-master/strace2csv.py "+self.outputDir+"/"+cmdName+".out -o "+self.outputDir+"/"+cmdName+".csv"))

			print(colored("VLCStraceGenerator: ", "blue")+colored("Done!", "green"))
                print(colored("VLCStraceGenerator: ", "blue")+"strace generation took: "+str(time.time()-self.time)+" secondes.")
